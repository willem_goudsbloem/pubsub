**Get a service key to use for restricting access**
curl -v 'http://localhost:8080/servicekey'
**returns:** 200 OK ```json { "key": "eOt95DHKrwZviNWR6qlX108V_YuDPVv6FUsjn7S6Pp0=", "creation_time": "0001-01-01T00:00:00Z", "expired": 24 }```

**Add topic 'mytopic'**  
curl -v -X PUT 'http://localhost:8080/topic/mytopic'  
**returns:** 201 CREATED ```json {"id":"mytopic","callback_domain":"127.0.0.1:41086","key":"<<topic_key>>'"}```  

**Modify topic 'mytopic'**  
curl -v -X PUT 'http://localhost:808l0/topic/mytopic?key=<<topic_key>>'
**returns:** 200 OK ```json {"id":"mytopic","callback_domain":"127.0.0.1:41086","key":"<<topic_key>>'"}```  

**Retrieve array with all topics**  
curl -v 'http://localhost:8080/topic?key=<<service_key>>'  
**returns:** 200 OK ```json [{"id":"test_topic_6129484611666145821","callback_domain":"","key":""},{"id":"test_topic_4037200794235010051","callback_domain":"","key":""}]```  

**Retrieve topic 'mytopic'**  
curl -v 'http://localhost:8080/topic/mytopic?key=<<topic_key>>'  
**returns:** 200 OK ```json {"id":"mytopic","callback_domain":"127.0.0.1:41086","key":""}```  

**Add subscription to topic: mytopic**  
curl -v -H "Content-Type: application/json" -d '[{"callback_url":'http://localhost:8082/test"}]' 'http://localhost:8080/topic/mytopic/subscription?key=<<topic_key>>'  
**returns:** 201 CREATED ```json [{"id":"gpVSs-jeAVCf55rDY75EB56xqPOf9zQzTBnTobvBGoY=","callback_url":'http://localhost:9090/cb"}]```  

**Retrieve array of all subscriptions belonging to 'mytopic'**  
curl -v 'http://localhost:8080/topic/mytopic/subscription?key=<<topic_key>>'  
**returns:** 200 OK ```json [{"id":"gpVSs-jeAVCf55rDY75EB56xqPOf9zQzTBnTobvBGoY=","callback_url":"http://localhost:9090/cb"}]```  

**Publish message to a subscriptions belonging to 'mytopic'**
curl -v -H "Content-Type: application/json" -d '{"content":"aGVsbG8gdGVzdCEhIQ=="}' 'http://localhost:8080/topic/mytopic/publish?key=<<topic_key>>'  

**Retrieve Published messages belonging to 'mytopic'**
curl -v 'http://localhost:8080/topic/mytopic/publish?key=<<topic_key>>'  

**Retrieve Notifications belonging to 'mytopic' and '<<subscriptionID>>' (optional) filtered by httpStatus**
curl -v 'http://localhost:8080/topic/mytopic/subscription/<<subscription_id>>/notification?httpStatus=200&key=<<topic_key>>'

204 (no content) when no notifications have been found with the given http status


**Run external test script**
run the build and execute ./test.sh in a command line

**Build docker image**  
```docker build -t wgoudsbloem/pubsub .```  

**Run docker image**  
```docker run -d -p 8081:8080 wgoudsbloem/pubsub pubsub```  
