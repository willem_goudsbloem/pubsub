package main

import (
	"os"
	"testing"
)

func init() {
	if err := os.Remove("store.db"); err != nil {
		if err.Error() != "remove store.db: no such file or directory" {
			log.fatal(err)
		}
	}
}

const (
	dummy_topicId = "abc123"
	dummy_keypass = "dummy_security_key"
)

func TestPutTopic(t *testing.T) {
	mockTopic := Topic{Id: dummy_topicId, CallBackDomain: "http://store_sqlite3_test", TopicKey: dummy_keypass}
	t.Logf("Storing into the datastore a mock Topic struct: %+v", mockTopic)
	ret, err := PutTopic(&mockTopic)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("Created = 1, Updated = 2, Result = %d", ret)
}

func TestGetTopics(t *testing.T) {
	t.Logf("retrieving mock Topics from the datastore")
	topics, err := GetTopics()
	if err != nil {
		t.Fatal(err)
	}
	if len(topics) < 1 {
		t.Error("no topics returned")
	}
	t.Logf("%d Topics found in the datastore: %+v", len(topics), topics)
}

func TestGetTopic(t *testing.T) {
	t.Logf("retrieving a topic from the data store with topic id %s", dummy_topicId)
	topic, err := GetTopic(dummy_topicId)
	if err != nil {
		t.Error(err)
	}
	if topic.Id != dummy_topicId {
		t.Errorf("expected topic with id: %s", dummy_topicId)
	}
	t.Logf("Topic found: %+v", topic)
}

func TestIsTopicByKey(t *testing.T) {
	isTopicByKey(dummy_topicId, dummy_keypass)
}

func TestDeleteTopic(t *testing.T) {
	topic := Topic{Id: "topicToBeDeleted"}
	t.Logf("Store Topic in datastore first: %+v", topic)
	topics := []Persistent{topic}
	ret, err := PutTopic(&topic)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("Created = 1, Udated = 2, Result = %d", ret)
	t.Logf("Delete topic from datastore: %+v", topic)
	rowsCount, err := Delete(topics)
	if err != nil {
		t.Fatal(err)
	}
	if rowsCount < 1 {
		t.Errorf("%d rows should have been deleted, but %d have been deleted", len(topics), rowsCount)
	}
}

func TestPutSubscription(t *testing.T) {
	subscription := Subscription{TopicId: "abc123", CallbackUrl: "/store/sqlite3/test/put"}
	t.Logf("Store Subscription in the datastore: %+v", subscription)
	err := PutSubscription(&subscription)
	if err != nil {
		t.Fatal(err)
	}
	if len(subscription.Id) == 0 {
		t.Fatal("expeced a subscription id back, but was empty")
	}
	t.Logf("subscription after put operation: %+v", subscription)
}

func TestGetSubscriptions(t *testing.T) {
	id := "abc123"
	subscriptions, err := GetSubscriptions(id)
	t.Logf("retrieve subscriptions from the datastore with id: %s", id)
	if err != nil {
		t.Error(err)
	}
	if len(subscriptions) < 1 {
		t.Fatal("no subscriptions found")
	}
	i := 0
	for _, subscription := range subscriptions {
		if subscription.TopicId == id {
			i++
		}
	}
	if i == 0 {
		t.Errorf("no subscription found for topic_id: %s", id)
	}
	t.Logf("Subscriptions found in the dtabase: %+v", subscriptions)
}

func TestPutMessage(t *testing.T) {
	content := `{"key":"value"}`
	msg := &Message{TopicId: "abc123", Content: []byte(content)}
	t.Logf("store a message in the datastore: %+v", msg)
	err := PutMessage(msg)
	if err != nil {
		t.Fatal(err)
	}
	if msg.Id == "" {
		t.Fatalf("message is not correct: %+v", msg)
	}
}

func TestDeleteSubscriptions(t *testing.T) {
	subscription := &Subscription{TopicId: "abc123", CallbackUrl: "/store/sqlite3/test/delete"}
	t.Logf("store to-be-deleted Subscription: %+v", subscription)
	err := PutSubscription(subscription)
	if err != nil {
		t.Fatal(err)
	}
	subscriptionIds := []string{subscription.Id}
	rowsDeleted, err := DeleteSubscriptions(subscriptionIds)
	if err != nil {
		t.Fatal(err)
	}
	if rowsDeleted < 1 {
		t.Error("No rows deleted (not found)")
	}
	t.Logf("%d row(s) deleted with subscription id: %v", rowsDeleted, subscriptionIds)
}

func TestGetMessage(t *testing.T) {
	var msgs []Message
	if err := GetMessages(&msgs, "abc123"); err != nil {
		t.Fatal(err)
	}
	if len(msgs) == 0 {
		t.Fatal("No Messages are found")
	}
}

func TestNotifyDbSuite(t *testing.T) {
	testPutNotifications(t, "sub345")
	testPutNotifications(t, "sub345")
	testGetNotifications(t, "sub345")
}

func testPutNotifications(t *testing.T, subscriptionId string) {
	notifications := []Notification{Notification{TopicId: "topic123", SubscriptionId: "sub123", HttpStatus: 202}, Notification{TopicId: "xyz123", SubscriptionId: subscriptionId, HttpStatus: 200}}
	t.Logf("store notifications in the datastore: %+v", notifications)
	err := PutNotifications(&notifications)
	if err != nil {
		t.Fatal(err)
	}
	if notifications[0].Id == "" {
		t.Fatal("no id return for %+v", notifications[0])
	}
}

func testGetNotifications(t *testing.T, subscriptionId string) {
	t.Logf("retrieve notifications with subscription id = %s", subscriptionId)
	notifications, err := GetNotifications(Subscription{Id: subscriptionId}, 200)
	if err != nil {
		t.Fatal(err)
	}
	if len(notifications) == 0 {
		t.Error("Not notifications found")
	}
	t.Log(notifications)
}

func TestServicekey(t *testing.T) {
	testkey := "Testkey_123"
	if err := putServicekey(&Servicekey{Key: testkey}); err != nil {
		t.Fatal(err)
	}
	servicekey := Servicekey{Key: testkey}
	if err := getServicekey(&servicekey); err != nil {
		t.Fatal(err)
	}
	if servicekey.Key != testkey {
		t.Fatalf("Expected key: %s, but got %s", testkey, servicekey.Key)
	}
	t.Log(servicekey)
}
