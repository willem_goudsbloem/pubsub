package main

import (
	"bytes"
	"io"
	"net/http"
	"sync"
	"time"
)

const (
	Duration = time.Second * 0
	Retries  = 0
)

type CallbackClient struct {
	*http.Client
	url string
}

// Way to get the callback client, for non testing set url to ""
func NewCallbackClient(url string) *CallbackClient {
	return &CallbackClient{&http.Client{}, url}
}

func (c *CallbackClient) post(url string, bodyType string, body io.Reader) (resp *http.Response, err error) {
	if c.url == "" {
		c.url = url
	}
	return c.Post(c.url, bodyType, body)
}

type callBackClient func(url string, bodyType string, body io.Reader) (resp *http.Response, err error)

func (fn callBackClient) Post(url string, bodyType string, body io.Reader) (resp *http.Response, err error) {
	return fn(url, bodyType, body)
}

func (c *CallbackClient) Notify(msg *Message) error {
	subscriptions, err := GetSubscriptions(msg.TopicId)
	if err != nil {
		return err
	}
	var notifications []Notification
	poster := func(subscription *Subscription) int {
		reader := bytes.NewReader(msg.Content)
		if res, err := c.post(subscription.CallbackUrl, "application/json", reader); err != nil {
			log.error(err)
			return 999
		} else {
			defer res.Body.Close()
			return res.StatusCode
		}
	}
	var wg sync.WaitGroup
	for i, subscription := range subscriptions {
		log.infof("processing subscription %d, value: %+v", i, subscription)
		wg.Add(1)
		go func(subscription *Subscription) {
			defer wg.Done()
			var status int
			for i := 0; Retries >= i; i++ {
				status = poster(subscription)
				if status != 200 {
					log.infof("status received %d, try again in %d seconds count nr. %d", status, Duration/time.Second, i+1)
					time.Sleep(Duration)
				} else {
					break
				}
				log.infof("status code of callbackserver %s = %d", subscription.CallbackUrl, status)
			}
			//register response
			notification := Notification{TopicId: msg.TopicId, SubscriptionId: subscription.Id, HttpStatus: status}
			notifications = append(notifications, notification)
		}(&subscription)
	}
	wg.Wait()
	err = PutNotifications(&notifications)
	if err != nil {
		return err
	}
	return nil
}
