package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

var router = mux.NewRouter().StrictSlash(true)

const (
	servicekeyUrl         = "/servicekey"
	topicUrl              = "/topic"
	topicUrlWithId        = "/topic/{topicId}"
	subscriptionUrl       = "/topic/{topicId}/subscription"
	subscriptionUrlWithId = "/topic/{topicId}/subscription/{subscriptionId}"
	publishUrl            = "/topic/{topicId}/publish"
	notificationUrl       = "/topic/{topicId}/subscription/{subscriptionId}/notification"
)

var port int
var debug bool
var keyDuration int64

func init() {
	flag.IntVar(&port, "port", 8080, "set the server port")
	flag.BoolVar(&debug, "debug", false, "turn on debugging messages to stdout")
	flag.Int64Var(&keyDuration, "kd", 24, "duration of the service key before expiry in hours")
	flag.Parse()
}

func main() {
	log.infof("starting server on port %d", port)
	http.Handle("/", router)
	router.HandleFunc(servicekeyUrl, servicekeyHandler).Methods("GET")
	router.HandleFunc(topicUrl, topicHandler)
	router.Handle(topicUrlWithId, authenticateHandler(topicHandler)).Queries("key", "value")
	router.Handle(topicUrlWithId, authenticateHandler(topicHandler)).Methods("GET")
	router.HandleFunc(topicUrlWithId, topicHandler).Methods("PUT")
	router.Handle(subscriptionUrl, authenticateHandler(subscriptionHandler))
	router.Handle(subscriptionUrlWithId, authenticateHandler(subscriptionHandler))
	router.Handle(publishUrl, authenticateHandler(publishMessageHandler))
	router.Handle(notificationUrl, authenticateHandler(notificationHandler)).Methods("GET")
	err := http.ListenAndServe(":"+strconv.Itoa(port), nil)
	if err != nil {
		panic(err)
	}
}

type authenticateHandler func(http.ResponseWriter, *http.Request)

func (fn authenticateHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	log.infof("%s\t%s%s", req.Method, req.Host, req.URL.String())
	topicId := mux.Vars(req)["topicId"]
	key := req.FormValue("key")
	if key == "" {
		res.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(res, "missing credentials")
		return
	}
	b, err := isTopicByKey(topicId, key)
	if err != nil {
		log.error(err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !b {
		res.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintln(res, "Credentials not valid")
		return
	}
	fn(res, req)
}

func servicekeyHandler(res http.ResponseWriter, req *http.Request) {
	servicekey := Servicekey{Key: uid(), Duration: keyDuration}
	json.NewEncoder(res).Encode(&servicekey)
}

//TODO Messy code, needs cleanup
func notificationHandler(res http.ResponseWriter, req *http.Request) {
	subscriptionId := mux.Vars(req)["subscriptionId"]
	var err error
	var notifications []Notification
	if req.FormValue("http_status") != "" {
		httpStatus, err := strconv.ParseInt(req.FormValue("http_status"), 10, 0)
		if err != nil {
			log.error(err)
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		notifications, err = GetNotifications(Subscription{Id: subscriptionId}, int(httpStatus))
	} else {
		notifications, err = GetNotifications(Subscription{Id: subscriptionId}, 0)
	}
	if err != nil {
		log.error(err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(notifications) == 0 {
		res.WriteHeader(http.StatusNoContent)
		return
	}
	err = json.NewEncoder(res).Encode(notifications)
	if err != nil {
		log.error(err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func publishMessageHandler(res http.ResponseWriter, req *http.Request) {
	topicId := mux.Vars(req)["topicId"]
	//check if topic_id is valid
	if status := checkTopic(topicId); status != -1 {
		res.WriteHeader(status)
		return
	}
	switch req.Method {
	case "POST":
		var msg Message
		if err := json.NewDecoder(req.Body).Decode(&msg); err != nil {
			log.error(err)
			res.WriteHeader(http.StatusBadRequest)
			fmt.Fprintln(res, "invalid json data received. format needed: {content}")
			return
		}
		msg.TopicId = topicId
		if err := PutMessage(&msg); err != nil {
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		if err := NewCallbackClient("").Notify(&msg); err != nil {
			log.error(err)
			res.WriteHeader(http.StatusInternalServerError)
		}
		//TODO wrong response below:
		// http://localhost:8080/topic/mytoplic/publish?key=N72G0dGbGkzTmAA7w1IQpj8xgrCcqIC6FksjgC1m1gA=/publish/S89UhdbxCjGQcuBnnUExYVNxNhfAlPZlQT38ITZrGMg=
		res.Header().Set("Location", transport(req)+req.Host+req.RequestURI+"/publish/"+msg.Id)
		res.WriteHeader(http.StatusCreated)
		return
	case "GET":
		var msgs []Message
		if err := GetMessages(&msgs, topicId); err != nil {
			res.WriteHeader(http.StatusInternalServerError)
		}
		if err := json.NewEncoder(res).Encode(msgs); err != nil {
			res.WriteHeader(http.StatusInternalServerError)
		}
	}
}

func subscriptionHandler(res http.ResponseWriter, req *http.Request) {
	topicId := mux.Vars(req)["topicId"]
	//check if topic_id is valid
	if status := checkTopic(topicId); status != -1 {
		res.WriteHeader(status)
		return
	}
	switch req.Method {
	case "POST":
		var subscriptions []Subscription
		if err := json.NewDecoder(req.Body).Decode(&subscriptions); err != nil {
			res.WriteHeader(http.StatusBadRequest)
			fmt.Fprintln(res, "no data found, minimum is: [{callback_url:<<url>>}]")
			return
		}
		for i := 0; i < len(subscriptions); i++ {
			subscriptions[i].TopicId = topicId
		}
		err := PutSubscriptions(&subscriptions)
		if err != nil {
			log.error(err)
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		res.WriteHeader(http.StatusCreated)
		json.NewEncoder(res).Encode(&subscriptions)
	case "DELETE":
		var subscriptionIds []string
		subscriptionId := mux.Vars(req)["subscriptionId"]
		//check if subscriptionId is in the url
		if subscriptionId != "" {
			subscriptionIds = append(subscriptionIds, subscriptionId)
			//otherwise it should be in the json body
		} else {
			if err := json.NewDecoder(req.Body).Decode(&subscriptionIds); err != nil {
				log.error(err)
				res.WriteHeader(http.StatusInternalServerError)
				return
			}
		}
		deleted, err := DeleteSubscriptions(subscriptionIds)
		if err != nil {
			log.error(err)
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		res.WriteHeader(http.StatusNoContent)
		json.NewEncoder(res).Encode(deleted)
	case "GET":
		subscriptions, err := GetSubscriptions(topicId)
		if err != nil {
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		if len(subscriptions) == 0 {
			res.WriteHeader(http.StatusNotFound)
			return
		}
		json.NewEncoder(res).Encode(subscriptions)
	default:
		res.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintln(res, "Methods supported are: POST and GET")
	}
}

//Implements PUT, DELETE and GET
func topicHandler(res http.ResponseWriter, req *http.Request) {
	//log.Printf("topicHandler() %s", req.Method)
	topicId := mux.Vars(req)["topicId"]
	topic := Topic{Id: topicId, CallBackDomain: req.RemoteAddr}
	switch req.Method {
	case "PUT":
		topic.TopicKey = uid()
		ret, err := PutTopic(&topic)
		if err != nil {
			log.error(err)
		}
		res.Header().Set("Location", transport(req)+req.Host+req.RequestURI)
		switch ret {
		case CREATED:
			res.WriteHeader(http.StatusCreated)
		case UPDATED:
			res.WriteHeader(http.StatusOK)
		}
		json.NewEncoder(res).Encode(topic)
	case "DELETE":
		_, err := Delete([]Persistent{topic})
		if err != nil {
			log.error(err)
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		res.WriteHeader(http.StatusNoContent)
	case "GET":
		if topic.Id == "" {
			topics, err := GetTopics()
			if err != nil {
				log.error(err)
			}
			if len(topics) > 0 {
				json.NewEncoder(res).Encode(topics)
			} else {
				res.WriteHeader(http.StatusNotFound)
			}
		} else {
			topic, err := GetTopic(topic.Id)
			if err != nil {
				log.error(err)
			}
			if topic != nil {
				json.NewEncoder(res).Encode(topic)
			} else {
				res.WriteHeader(http.StatusNotFound)
			}

		}
	default:
		res.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintln(res, "Methods supported are: PUT GET DELETE")
	}
}

//TODO Move below funcs to helper file?
func checkTopic(topicId string) (httpStatus int) {
	topic, err := GetTopic(topicId)
	if err != nil {
		log.error(err)
		return http.StatusInternalServerError
	}
	if topic == nil {
		return http.StatusNotFound

	}
	return -1
}

func transport(req *http.Request) string {
	if req.TLS != nil {
		return "https://"
	}
	return "http://"
}
