package main

import (
	"fmt"
	_log "log"
	"os"
	"runtime"
	"strings"
)

type logger struct{}

var log logger

func (l *logger) debug(i interface{}) {
	if debug {
		pc := make([]uintptr, 10) // at least 1 entry needed
		runtime.Callers(2, pc)
		f := runtime.FuncForPC(pc[0])
		file, line := f.FileLine(pc[0])
		paths := strings.Split(file, "/")
		_log.Printf("DEBUG:\t%s:%d %s\t%v\n", paths[len(paths)-1], line, f.Name(), i)
	}
}
func (l *logger) debugf(format string, v ...interface{}) {
	if debug {
		i := fmt.Sprintf(format, v)
		pc := make([]uintptr, 10) // at least 1 entry needed
		runtime.Callers(2, pc)
		f := runtime.FuncForPC(pc[0])
		file, line := f.FileLine(pc[0])
		paths := strings.Split(file, "/")
		_log.Printf("DEBUG:\t%s:%d %s\t%v\n", paths[len(paths)-1], line, f.Name(), i)
	}
}

func (l *logger) fatal(i interface{}) {
	pc := make([]uintptr, 10) // at least 1 entry needed
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])
	file, line := f.FileLine(pc[0])
	paths := strings.Split(file, "/")
	_log.Printf("FATAL:\t%s:%d %s\t%v\n", paths[len(paths)-1], line, f.Name(), i)
	os.Exit(0)
}

func (l *logger) info(i ...interface{}) {
	pc := make([]uintptr, 10) // at least 1 entry needed
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])
	file, line := f.FileLine(pc[0])
	paths := strings.Split(file, "/")
	_log.Printf("INFO:\t%s:%d %s\t%v\n", paths[len(paths)-1], line, f.Name(), i)
}

func (l *logger) infof(format string, v ...interface{}) {
	i := fmt.Sprintf(format, v...)
	pc := make([]uintptr, 10) // at least 1 entry needed
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])
	file, line := f.FileLine(pc[0])
	paths := strings.Split(file, "/")
	_log.Printf("INFO:\t%s:%d %s\t%v\n", paths[len(paths)-1], line, f.Name(), i)
}

func (l *logger) error(i interface{}) {
	pc := make([]uintptr, 10) // at least 1 entry needed
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])
	file, line := f.FileLine(pc[0])
	paths := strings.Split(file, "/")
	_log.Printf("ERROR:\t%s:%d %s\t%v\n", paths[len(paths)-1], line, f.Name(), i)
}

func (l *logger) errorf(format string, v ...interface{}) {
	i := fmt.Sprintf(format, v)
	pc := make([]uintptr, 10) // at least 1 entry needed
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])
	file, line := f.FileLine(pc[0])
	paths := strings.Split(file, "/")
	_log.Printf("ERROR:\t%s:%d %s\t%v\n", paths[len(paths)-1], line, f.Name(), i)
}
