package main

import (
	"crypto/rand"
	"database/sql"
	"encoding/base64"
	"fmt"
	"strings"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

var _db *sql.DB

func db() *sql.DB {
	if _db != nil {
		return _db
	}
	var err error
	_db, err = sql.Open("sqlite3", "./store.db")
	if err != nil {
		panic(err)
	}
	_db.Exec("create table topics (id string unique, callback_domain string, keypass string, timestamp text)")
	_db.Exec("create table subscriptions (id string, topic_id string, callback_url string, timestamp text)")
	_db.Exec("create table messages (id string, topic_id string, content blob, timestamp text)")
	_db.Exec("create table notifications (id string, topic_id string, subscription_id string, http_status int, timestamp text)")
	_db.Exec("create table servicekey (key string, created text, expired int64)")
	return _db
}

const (
	CREATED = 1
	UPDATED = 2
)

// Store topic
func PutTopic(topic *Topic) (int, error) {
	ret := CREATED
	db := db()
	tx, err := db.Begin()
	if err != nil {
		return 0, err
	}
	// check for duplicates
	_topic, err2 := GetTopic(topic.Id)
	if err2 != nil {
		return 0, err2
	}
	if _topic != nil {
		ret = UPDATED
	}
	stmt, err := tx.Prepare("insert or replace into topics (id, callback_domain, keypass, timestamp) values (?, ?, ?, ?)")
	if err != nil {
		return 0, err
	}
	defer stmt.Close()
	_, err3 := stmt.Exec(topic.Id, topic.CallBackDomain, topic.TopicKey, time.Now())
	if err3 != nil {
		return 0, err3
	}
	tx.Commit()
	return ret, nil
}

// Returns all topics in the database
func GetTopics() ([]Topic, error) {
	var topics []Topic
	db := db()
	rows, err := db.Query("select id, callback_domain from topics")
	if err != nil {
		return topics, err
	}
	defer rows.Close()
	for rows.Next() {
		var id, callbackDomain string
		err = rows.Scan(&id, &callbackDomain)
		if err != nil {
			return topics, nil
		}
		topic := Topic{Id: id, CallBackDomain: callbackDomain}
		topics = append(topics, topic)
	}
	return topics, nil
}

// Returns specific topic by id
func GetTopic(id string) (topic *Topic, err error) {
	db := db()
	rows, err := db.Query("select id, callback_domain from topics where id = ?", id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if rows.Next() {
		var _id, callbackDomain string
		rows.Scan(&_id, &callbackDomain)
		topic = &Topic{Id: _id, CallBackDomain: callbackDomain}
		return topic, nil
	}
	return nil, nil
}

func isTopicByKey(topicId string, key string) (bool, error) {
	db := db()
	rows, err := db.Query("select 1 from topics where id = ? and keypass = ?", topicId, key)
	if err != nil {
		return false, err
	}
	defer rows.Close()
	if rows.Next() {
		return true, nil
	}
	return false, nil
}

// add the passed subscription to the database
func PutSubscription(subscription *Subscription) error {
	db := db()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	stmt, err := tx.Prepare("insert into subscriptions (id, topic_id, callback_url, timestamp) values (?, ?, ?, ?)")
	if err != nil {
		return err
	}
	defer stmt.Close()
	uid := uid()
	_, err = stmt.Exec(uid, subscription.TopicId, subscription.CallbackUrl, time.Now())
	if err != nil {
		return err
	}
	err = tx.Commit()
	if err != nil {
		return err
	}
	subscription.Id = uid
	return nil
}

// get an array of subscriptions by a given topic id
func GetSubscriptions(topicId string) ([]Subscription, error) {
	var subscriptions []Subscription
	db := db()
	stmt, err := db.Prepare("select * from subscriptions where topic_id = ?")
	if err != nil {
		return subscriptions, err
	}
	defer stmt.Close()
	rows, err := stmt.Query(topicId)
	if err != nil {
		return subscriptions, err
	}
	defer rows.Close()
	for rows.Next() {
		var subscription Subscription
		var timestamp string
		err = rows.Scan(&subscription.Id, &subscription.TopicId, &subscription.CallbackUrl, &timestamp)
		if err != nil {
			return subscriptions, err
		}
		ts, err := time.Parse("2006-01-02 15:04:05-07:00", timestamp)
		if err != nil {
			return subscriptions, err
		}
		subscription.Timestamp = ts
		subscriptions = append(subscriptions, subscription)
	}
	return subscriptions, nil
}

func PutSubscriptions(subscriptions *[]Subscription) error {
	db := db()
	for i, _ := range *subscriptions {
		stmt, err := db.Prepare("insert into subscriptions values(?,?,?,?)")
		if err != nil {
			return err
		}
		(*subscriptions)[i].Id = uid()
		_, err = stmt.Exec((*subscriptions)[i].Id, (*subscriptions)[i].TopicId, (*subscriptions)[i].CallbackUrl, time.Now())
		if err != nil {
			return err
		}
	}
	return nil
}

func Delete(ps []Persistent) (int64, error) {
	db := db()
	var ids []string
	for _, p := range ps {
		ids = append(ids, p.getId())
	}
	stmt := fmt.Sprintf("delete from %s where id in ('%s')", ps[0].getTableName(), strings.Join(ids, "','"))
	//log.error(stmt)
	res, err := db.Exec(stmt)
	if err != nil {
		return 0, err
	}
	rowsDeleted, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	//log.Printf("%d rows deleted, ids %v", rowsDeleted, ids)
	return rowsDeleted, nil
}

//TODO Is this safe (SQL injection?)
func DeleteSubscriptions(subscriptionIds []string) (int64, error) {
	db := db()
	stmt := fmt.Sprintf("delete from subscriptions where id in ('%s')", strings.Join(subscriptionIds, "','"))
	//log.error(stmt)
	res, err := db.Exec(stmt)
	if err != nil {
		return 0, err
	}
	rowsDeleted, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	//log.Printf("%d rows deleted, ids %v", rowsDeleted, subscriptionIds)
	return rowsDeleted, nil
}

// store a message in the datastore
func PutMessage(msg *Message) error {
	db := db()
	stmt, err := db.Prepare("insert into messages values(?,?,?,?)")
	if err != nil {
		return err
	}
	msg.Id = uid()
	_, err = stmt.Exec(msg.Id, msg.TopicId, msg.Content, time.Now())
	if err != nil {
		return err
	}
	return nil
}

// Returns all topics in the database
func GetMessages(msgs *[]Message, topicId string) error {
	db := db()
	stmt, err := db.Prepare("select * from messages where topic_id = ?")
	if err != nil {
		return err
	}
	defer stmt.Close()
	rows, err := stmt.Query(topicId)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		var id, topicId, timestamp string
		var content []byte
		err = rows.Scan(&id, &topicId, &content, &timestamp)
		if err != nil {
			return err
		}
		ts, err := time.Parse("2006-01-02 15:04:05-07:00", timestamp)
		if err != nil {
			return err
		}
		msg := Message{Id: id, TopicId: topicId, Content: content, Timestamp: ts}
		*msgs = append(*msgs, msg)
	}
	return nil
}

// store a notification in the datastore
func PutNotifications(notifications *[]Notification) error {
	db := db()
	for i, _ := range *notifications {
		stmt, err := db.Prepare("insert into notifications (id, topic_id, subscription_id, http_status, timestamp) values(?,?,?,?,?)")
		if err != nil {
			log.error(err)
			return err
		}
		(*notifications)[i].Id = uid()
		_, err = stmt.Exec((*notifications)[i].Id, (*notifications)[i].TopicId, (*notifications)[i].SubscriptionId, (*notifications)[i].HttpStatus, time.Now())
		if err != nil {
			log.error(err)
			return err
		}
	}
	return nil
}

func GetNotifications(subscription Subscription, httpStatus int) (notifications []Notification, err error) {
	db := db()
	log.debugf("subscription: %+v", subscription)
	var rows *sql.Rows
	if httpStatus != 0 {
		rows, err = db.Query("select * from notifications where subscription_id = ? and http_status = ?", subscription.Id, httpStatus)
	} else {
		rows, err = db.Query("select * from notifications where subscription_id = ?", subscription.Id)
	}
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		n := Notification{}
		//id string, topic_id string, subscription_id string, http_status int, timestamp text
		var timestampTxt string
		rows.Scan(&n.Id, &n.TopicId, &n.SubscriptionId, &n.HttpStatus, &timestampTxt)
		if n.Timestamp, err = parseTime(timestampTxt); err != nil {
			log.error(err)
		} else {
			notifications = append(notifications, n)
		}
	}
	log.debugf("notifications found: %v", notifications)
	return notifications, nil
}

func putServicekey(servicekey *Servicekey) error {
	db := db()
	log.debug(servicekey)
	if _, err := db.Exec("insert into servicekey values (?,?,?)", servicekey.Key, time.Now(), servicekey.Duration); err != nil {
		return err
	}
	return nil
}

func getServicekey(servicekey *Servicekey) error {
	db := db()
	log.debug(servicekey)
	if result, err := db.Query("select * from servicekey where key = ?", servicekey.Key); err != nil {
		return err
	} else {
		for result.Next() {
			var created string
			if err := result.Scan(&servicekey.Key, &created, &servicekey.Duration); err != nil {
				return err
			} else {
				var err error
				if servicekey.Created, err = parseTime(created); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

//helper func
//TODO move to own helper file?
func uid() string {
	b := make([]byte, 32)
	rand.Read(b)
	return base64.URLEncoding.EncodeToString(b)
}

func parseTime(timestring string) (t time.Time, err error) {
	ts, err := time.Parse("2006-01-02 15:04:05-07:00", timestring)
	if err != nil {
		return t, err
	}
	return ts, nil
}
