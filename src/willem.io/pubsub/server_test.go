package main

import (
	"bytes"
	"encoding/json"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"os"
	"strconv"
	"testing"
)

func init() {
	if err := os.Remove("store.db"); err != nil {
		if err.Error() != "remove store.db: no such file or directory" {
			log.fatal(err)
		}
	}
}

func testTopicId() string {
	return "test_topic_" + strconv.Itoa(rand.Int())
}

func TestTopicHandlerSuite(t *testing.T) {
	// PUT /topic/{topic id} = create topic resource
	testTopicId1 := testTopicId()
	testTopicPut(t, testTopicId1)
	// PUT /topic/{topic id} = create topic resource
	testTopicId2 := testTopicId()
	keypass2 := testTopicPut(t, testTopicId2)
	// GET /topic = get all topics
	testTopicsGet(t, testTopicId1)
	// GET /topic/{topic id} = get single topic
	testTopicGet(t, testTopicId2, keypass2)
	// DELETE /topic/{topic id} = delete a single topic resource
	testTopicDelete(t, testTopicId1)
	// DELETE /topic/{topic id} = delete a single topic resource
	testTopicDelete(t, testTopicId2)
}

func TestSubscriptionHandlerSuite(t *testing.T) {
	// Create a topic for this suite
	testTopicId1 := testTopicId()
	keypass1 := testTopicPut(t, testTopicId1)
	// POST /topic/{topic id}/subscription = add a subscription
	testSubscriptionPost(t, testTopicId1, keypass1)
	// GET /topic/{topic id}/subscription = get all subscription by topic
	testSubscriptionGet(t, testTopicId1, keypass1)
	// DELETE /topic/{topic id}/subscription/{subscription id} = delete a subscription
	testSubscriptionDelete(t, testTopicId1, keypass1)
}

func TestPublishHandlerSuite(t *testing.T) {
	// PUT /topic/{topic id} = create topic resource
	testTopicId1 := testTopicId()
	keypass1 := testTopicPut(t, testTopicId1)
	// POST /topic/{topic id}/publish = publish a message
	testPublishPost(t, testTopicId1, keypass1)
	// POST /topic/{topic id}/publish = publish a message
	testPublishPost(t, testTopicId1, keypass1)
	// GET /topic/{topic id}/publish = get all published messages
	testPublishGet(t, testTopicId1, keypass1)
}

func TestNotifyHandlerSuite(t *testing.T) {
	// PUT /topic/{topic id} = create topic resource
	testTopicId1 := testTopicId()
	keypass1 := testTopicPut(t, testTopicId1)
	// POST /topic/{topic id}/subscription = add a subscription
	testSubscriptionId := testSubscriptionPost(t, testTopicId1, keypass1)
	// POST /topic/{topic id}/publish = publish a message
	testPublishPost(t, testTopicId1, keypass1)
	testNotificationsGet(t, testTopicId1, testSubscriptionId, keypass1)
}

func TestServicekeyHandler(t *testing.T) {
	t.Logf("GET\t\tservicekey")
	req, err := http.NewRequest("GET", "/servicekey", nil)
	if err != nil {
		t.Fatal(err)
	}
	res := httptest.NewRecorder()
	router.HandleFunc("/servicekey", servicekeyHandler)
	router.ServeHTTP(res, req)
	if res.Code != http.StatusOK {
		t.Fatalf("FAIL:\t\tExpected http %d but got %d", http.StatusOK, res.Code)
	}
	if res.Body.Len() == 0 {
		t.Fatal("Res.Body is empty")
	}
	var servicekey Servicekey
	if err := json.NewDecoder(res.Body).Decode(&servicekey); err != nil {
		t.Fatal(err)
	}
	if servicekey.Key == "" {
		t.Fatalf("expected a key but got none")
	}
}

func testNotificationsGet(t *testing.T, testTopicId string, testSubscriptionId string, keypass string) {
	t.Logf("GET\t\t/topic/%s/subscription/%s/notification?key=%s", testTopicId, testSubscriptionId, keypass)
	req, err := http.NewRequest("GET", "/topic/"+testTopicId+"/subscription/"+testSubscriptionId+"/notification?key="+keypass, nil)
	if err != nil {
		t.Fatal(err)
	}
	res := httptest.NewRecorder()
	router.HandleFunc("/topic/{topicid}/subscription/{subscriptionId}/notification", notificationHandler)
	router.ServeHTTP(res, req)
	if res.Code != http.StatusOK {
		t.Fatalf("FAIL:\t\tExpected http %d, but got %d", http.StatusOK, res.Code)
	}
	t.Logf("res.body\t\t%+v", res.Body)
}

func testPublishGet(t *testing.T, testTopicId string, keypass string) {
	t.Logf("GET\t\t/topic/%s/publish?key=%s", testTopicId, keypass)
	req, err := http.NewRequest("GET", "/topic/"+testTopicId+"/publish?key="+keypass, nil)
	if err != nil {
		t.Fatal(err)
	}
	res := httptest.NewRecorder()
	router.Handle("/topic/{topicid}/publish", authenticateHandler(subscriptionHandler))
	router.ServeHTTP(res, req)
	if res.Code != http.StatusOK {
		t.Fatalf("FAIL:\t\tExpected http %d, but got %d", http.StatusOK, res.Code)
	}
	t.Logf("res.body\t\t%+v", res.Body)
	var messages []Message
	if err := json.NewDecoder(res.Body).Decode(&messages); err != nil {
		t.Fatal(err)
	}
	t.Logf("messages\t\t%+v", messages)
	t.Logf("content\t\t%s", string(messages[0].Content))
}

func testSubscriptionGet(t *testing.T, testTopicId string, keypass string) {
	t.Logf("GET\t\t/topic/%s/subscription?key=%s", testTopicId, keypass)
	req, err := http.NewRequest("GET", "/topic/"+testTopicId+"/subscription?key="+keypass, nil)
	if err != nil {
		t.Fatal(err)
	}
	res := httptest.NewRecorder()
	router.Handle("/topic/{topicid}/subscription", authenticateHandler(subscriptionHandler))
	router.ServeHTTP(res, req)
	if res.Code != http.StatusOK {
		t.Fatalf("FAIL:\t\tExpected http %d, but got %d", http.StatusOK, res.Code)
	}
	var subscriptions []Subscription
	if err := json.NewDecoder(res.Body).Decode(&subscriptions); err != nil {
		t.Fatal(err)
	}
	if len(subscriptions) == 0 {
		t.Fatal("FAIL:\t\tshould expect to get an array of subscriptions")
	}
	t.Logf("subscriptions:\t%+v", subscriptions)
}

func testPublishPost(t *testing.T, testTopicId string, keypass string) {
	content := []byte("testContent_here")
	msg := Message{Content: content}
	t.Logf("POST\t\ttopic/%s/publish?key=%s %+v", testTopicId, keypass, msg)
	bw := new(bytes.Buffer)
	if err := json.NewEncoder(bw).Encode(&msg); err != nil {
		t.Error(err)
	}
	req, err := http.NewRequest("POST", "/topic/"+testTopicId+"/publish?key="+keypass, bw)
	if err != nil {
		t.Fatal(err)
	}
	res := httptest.NewRecorder()
	router.Handle("/topic/{topicId}/publish", authenticateHandler(publishMessageHandler))
	router.ServeHTTP(res, req)
	if status := res.Code; status != http.StatusCreated {
		t.Errorf("FAIL:\t\thandler returned wrong status code: got %v want %v",
			status, http.StatusCreated)
	}
	if res.Header().Get("Location") == "" {
		t.Error("Expecting a location header")
	}
	t.Logf("location header:\t%+v", res.Header().Get("Location"))
}

func testSubscriptionDelete(t *testing.T, testTopicId string, keypass string) {
	testSubscriptionId := "test_subscription_id_456"
	t.Logf("DELETE\t\t/topic/%s/subscription/%s?key=%s", testTopicId, testSubscriptionId, keypass)
	req, err := http.NewRequest("DELETE", "/topic/"+testTopicId+"/subscription/"+testSubscriptionId+"?key="+keypass, nil)
	if err != nil {
		t.Fatal(err)
	}
	res := httptest.NewRecorder()
	router.Handle("/topic/{topicId}/subscription/{subscriptionId}", authenticateHandler(subscriptionHandler))
	router.ServeHTTP(res, req)
	if res.Code != http.StatusNoContent {
		t.Errorf("FAIL:\t\texpected http code: %d, but got %d", http.StatusNoContent, res.Code)
	}
}

func testSubscriptionPost(t *testing.T, testTopicId string, keypass string) string {
	t.Logf("POST\t\t/topic/%s/subscription?key=%s", testTopicId, keypass)
	subscriptions := []Subscription{Subscription{CallbackUrl: "/server/test/post"}}
	bw := new(bytes.Buffer)
	if err := json.NewEncoder(bw).Encode(&subscriptions); err != nil {
		t.Error(err)
	}
	req, err := http.NewRequest("POST", "/topic/"+testTopicId+"/subscription?key="+keypass, bw)
	if err != nil {
		t.Fatal(err)
	}
	res := httptest.NewRecorder()
	router.Handle("/topic/{topicId}/subscription", authenticateHandler(subscriptionHandler))
	router.ServeHTTP(res, req)
	if status := res.Code; status != http.StatusCreated {
		t.Errorf("FAIL:\t\thandler returned wrong status code: got %v want %v",
			status, http.StatusCreated)
	}
	t.Logf("response body:\t%+v", res.Body)
	json.NewDecoder(res.Body).Decode(&subscriptions)
	if subscriptions[0].Id == "" {
		t.Fatal("FAIL:\t\tsubscription id suppose to be a value, but was empty")
	}
	return subscriptions[0].Id
}

func testTopicPut(t *testing.T, testTopicId string) (keypass string) {
	t.Logf("PUT\t\t/topic/%s", testTopicId)
	b := new(bytes.Buffer)
	topic := Topic{}
	if err := json.NewEncoder(b).Encode(topic); err != nil {
		t.Fatal(err)
	}
	req, err := http.NewRequest("PUT", "/topic/"+testTopicId, b)
	if err != nil {
		t.Fatal(err)
	}
	res := httptest.NewRecorder()
	req.RemoteAddr = "http://bogushost:9999"
	//need to use a mux router
	router.HandleFunc("/topic/{topicId}", topicHandler)
	router.ServeHTTP(res, req)
	if status := res.Code; status != http.StatusCreated {
		t.Errorf("FAIL:\t\thandler returned wrong status code: got %v want %v",
			status, http.StatusCreated)
	}
	if err := json.NewDecoder(res.Body).Decode(&topic); err != nil {
		t.Fatal(err)
	}
	if topic.TopicKey == "" {
		t.Fatal("no key found")
	}
	t.Logf("Keypass:\t\t%+v", topic.TopicKey)
	return topic.TopicKey
}

// Return all topics
func testTopicsGet(t *testing.T, testTopicId string) {
	t.Logf("GET\t\t/topic")
	req2, err2 := http.NewRequest("GET", "/topic?key=", nil)
	if err2 != nil {
		t.Fatal(err2)
	}
	res2 := httptest.NewRecorder()
	//need to use a mux router
	router.HandleFunc("/topic", topicHandler)
	router.ServeHTTP(res2, req2)
	if res2.Code != http.StatusOK {
		t.Fatalf("\t\t\tExpected http status code: %d but got: %d", http.StatusOK, res2.Code)
	}
	var topics []Topic
	err := json.NewDecoder(res2.Body).Decode(&topics)
	if err != nil {
		t.Fatal(err)
	}
	i := 0
	for _, topic := range topics {
		if topic.Id == testTopicId {
			i++
		}
	}
	if i == 0 {
		t.Errorf("FAIL:\t\twas expecting %s, but could not found in %v", testTopicId, topics)
	}
	if i > 1 {
		t.Fatal("FAIL:\t\tduplicate topic_id, should not be allowed: %+v", topics)
	}
	t.Logf("topics:\t\t%+v", topics)
}

func testTopicGet(t *testing.T, testTopicId string, keypass string) {
	t.Logf("GET\t\t/topic/%s?key=%s", testTopicId, keypass)
	req, err := http.NewRequest("GET", "/topic/"+testTopicId+"?key="+keypass, nil)
	if err != nil {
		t.Fatal(err)
	}
	res := httptest.NewRecorder()
	//need to use a mux router
	router.HandleFunc("/topic/{topicId}", authenticateHandler(topicHandler))
	router.ServeHTTP(res, req)
	var topic Topic
	err = json.NewDecoder(res.Body).Decode(&topic)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("topic:\t\t%+v", topic)
}

func testTopicDelete(t *testing.T, testTopicId string) {
	t.Logf("DELETE\t\t/topic/%s", testTopicId)
	req3, err3 := http.NewRequest("DELETE", "/topic/"+testTopicId, nil)
	if err3 != nil {
		t.Fatal(err3)
	}
	res3 := httptest.NewRecorder()
	//need to use a mux router
	router.Handle("/topic", authenticateHandler(topicHandler))
	router.ServeHTTP(res3, req3)
	if res3.Code != http.StatusNoContent {
		t.Errorf("FAIL:\t\texpected http code: %d, but got %d", http.StatusNoContent, res3.Code)
	}
}
