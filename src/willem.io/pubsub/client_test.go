package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func init() {
	if err := os.Remove("store.db"); err != nil {
		if err.Error() != "remove store.db: no such file or directory" {
			log.fatal(err)
		}
	}
}

const (
	testTopicId2 = "test_topic_id_456"
	serverPort   = ":9991"
	callBackUrl  = "http://localhost" + serverPort
	testMessage  = "Test_Content_xyz"
)

func TestNotifySuite(t *testing.T) {
	if err := PutSubscription(&Subscription{TopicId: testTopicId2, CallbackUrl: callBackUrl}); err != nil {
		t.Fatal(err)
	}
	if err := PutSubscription(&Subscription{TopicId: testTopicId2, CallbackUrl: callBackUrl}); err != nil {
		t.Fatal(err)
	}
	testNotify200(t)
	testNotify500(t)
}

func testNotify200(t *testing.T) {
	t.Log("testNotify with return status code 200")
	msg := Message{TopicId: testTopicId2, Content: []byte(testMessage)}
	if err := PutMessage(&msg); err != nil {
		t.Fatal(err)
	}
	ts := httptest.NewServer(http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		body, err := ioutil.ReadAll(req.Body)
		if err != nil {
			t.Fatal(err)
		}
		if string(body) != testMessage {
			t.Errorf("expect test message: %s, but got %s", testMessage, body)
		}
		t.Logf("%+v", string(body))
		res.WriteHeader(http.StatusOK)
		fmt.Fprintln(res, "Hello, client")
	}))
	defer ts.Close()
	client := NewCallbackClient(ts.URL)
	if err := client.Notify(&msg); err != nil {
		t.Fatal(err)
	}
}

func testNotify500(t *testing.T) {
	t.Log("testNotify with return status code 500")
	msg := Message{TopicId: testTopicId2, Content: []byte(testMessage)}
	if err := PutMessage(&msg); err != nil {
		t.Fatal(err)
	}
	ts := httptest.NewServer(http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		body, err := ioutil.ReadAll(req.Body)
		if err != nil {
			t.Fatal(err)
		}
		if string(body) != testMessage {
			t.Errorf("expect test message: %s, but got %s", testMessage, body)
		}
		t.Logf("%+v", string(body))
		res.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintln(res, "Hello, client")
	}))
	defer ts.Close()
	client := NewCallbackClient(ts.URL)
	if err := client.Notify(&msg); err != nil {
		t.Fatal(err)
	}
}
