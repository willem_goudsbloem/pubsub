package main

import (
	"time"
)

type Persistent interface {
	getId() string
	getTableName() string
}

type Topic struct {
	Id             string    `json:"id"`
	CallBackDomain string    `json:"callback_domain"`
	TopicKey       string    `json:"key"`
	Timestamp      time.Time `json:"timestamp"`
}

func (t Topic) getId() string {
	return t.Id
}

func (t Topic) getTableName() string {
	return "topics"
}

type Message struct {
	Id        string    `json:"id"`
	TopicId   string    `json:"-"`
	Content   []byte    `json:"content"`
	Timestamp time.Time `json:"timestamp"`
}

type PublishResponseMessage struct {
	Resource string `json: "resource"`
}

type Subscription struct {
	Id          string    `json:"id"`
	TopicId     string    `json:"-"`
	CallbackUrl string    `json:"callback_url"`
	Timestamp   time.Time `json:"timestamp"`
}

func (t Subscription) getId() string {
	return t.Id
}

func (t Subscription) getTableName() string {
	return "topics"
}

type Notification struct {
	Id             string    `json:"id"`
	TopicId        string    `json:"topic_id"`
	SubscriptionId string    `json:"subscription_id"`
	HttpStatus     int       `json:"http_status"`
	Timestamp      time.Time `json:"timestamp"`
}

type Servicekey struct {
	Key      string    `json:"key"`
	Created  time.Time `json:"creation_time"`
	Duration int64     `json:"expired"`
}
