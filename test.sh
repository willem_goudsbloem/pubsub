#!/bin/bash

echo 'run test make sure you have jq installed (sudo apt-get install jq)'

# Black        0;30     Dark Gray     1;30
# Red          0;31     Light Red     1;31
# Green        0;32     Light Green   1;32
# Brown/Orange 0;33     Yellow        1;33
# Blue         0;34     Light Blue    1;34
# Purple       0;35     Light Purple  1;35
# Cyan         0;36     Light Cyan    1;36
# Light Gray   0;37     White         1;37

RED='\033[0;31m'
BLUE='\033[0;34m'
NC='\033[0m'

url0='http://localhost:8080/servicekey'
echo -e $RED $url0 $NC

servicekey=$(curl -v $url0 | jq .)

echo -e $BLUE "servicekey:" $servicekey $NC "\n"

url1='http://localhost:8080/topic/mytopic'
echo -e $RED $url1 $NC

key=$(curl -v -X PUT $url1 | jq -r '.key')

url2="http://localhost:8080/topic/mytopic/subscription?key=$key"
echo -e $RED $url2 $NC
data1='[{"callback_url":"http://localhost:8082/test"}]'
echo -e $RED $data $NC

subscription_id=$(curl -v -H "Content-Type: application/json" \
-d $data1 \
$url2 | jq -r .[0].id)

url3="http://localhost:8080/topic/mytopic/publish?key=$key"
echo -e $RED $url3 $NC
data2='{"content":"aGVsbG8gdGVzdCEhIQ=="}'
echo -e $RED $data2 $NC

curl -v -H "Content-Type: application/json" \
-d $data2 \
$url3

url4="http://localhost:8080/topic/mytopic/subscription/$subscription_id/notification?&key=$key"
echo -e $RED $url4 $NC

curl -v $url4 | jq .

