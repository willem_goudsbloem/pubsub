FROM ubuntu:latest
MAINTAINER Willem Goudsbloem <willem@willem.io>
RUN apt-get update && apt-get install -y sqlite3
COPY bin/pubsub /usr/local/bin/
EXPOSE 8080